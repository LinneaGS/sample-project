
\documentclass[12pt,a4paper]{article}

\RequirePackage[l2tabu, orthodox]{nag}

\usepackage{silence}
\WarningFilter[pdftoc]{hyperref}{Token not allowed in a PDF string}
\ActivateWarningFilters[pdftoc]

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
%\usepackage[francais]{babel}

\newcommand \versionno{\jobname.tex}

\usepackage[margin=20mm]{geometry}

\usepackage{showkeys}

\usepackage{amsmath,amssymb,amsfonts,amscd,latexsym,amsthm}

\newtheoremstyle{break}{9pt}{9pt}{\itshape}{}{\bfseries}{}{\newline}{}
\theoremstyle{break}    
\newtheorem{hyp}{Hypothesis}[section]
\newtheorem{defn}[hyp]{Definition}

\usepackage{pgf}
\usepackage{tikz}

\usepackage[colorlinks=true, linktoc=all, linkcolor=blue, citecolor=red, urlcolor=blue, backref=page]{hyperref}

\usepackage{etoolbox}
\makeatletter
\patchcmd{\BR@backref}{\newblock}{\newblock(}{}{}
\patchcmd{\BR@backref}{\par}{)\par}{}{}
\makeatother

\numberwithin{equation}{section}
\setcounter{secnumdepth}{2}
\setcounter{tocdepth}{2}

\begin{document}

\typeout{}\typeout{\versionno}\typeout{} \begin{center} \fbox{\texttt{\versionno\ -- } {\small \today\ }} \end{center}

\vspace{2mm}

\begin{center}
 \textit{ \Huge Diagrammatics for correlation functions\\ in the $O(n)$ and Potts models}
\end{center}


\hypersetup{linkcolor=black}

\tableofcontents

\hypersetup{linkcolor=blue}


\section{Introduction}

Inspired by the lattice definition of the Potts and $O(n)$ model, we try to give qualitative predictions for the correlation functions of the corresponding CFTs. We leave aside the question of precisely justifying the diagrammatics from the lattice, with the idea of testing their validity using the conformal bootstrap. 

In principle we would like to make predictions on representations of the Virasoro symmetry, on the global symmetry i.e. $O(n)$ or $S_Q$, and even on the hypothetical full symmetry algebra. For the moment the predictions are about the Virasoro symmetry alone.  

\section{$O(n)$ model}

Notations: we label Virasoro representations with Kac table indices $(r, s)$. The relation with the representations of the affine Temperley--Lieb algebra parametrized by $j,z$ is, in the case of a non-diagonal field with $j\neq 0$, 
\begin{align}
 s & = j \\
 r & = e_\phi - 2e
\end{align}
where $z=e^{i\pi e_\phi}$ is such that $z^{2j}=1$ and where $e \in \mathbb{Z}$. For the diagonal fields with $j=0$ the relation is rather
\begin{align}
 s & = 1 \\
 r & = 1+2e
\end{align}
with $e\in \mathbb{Z}$.

The primary fields of the $O(n)$ model are 
\begin{align}
 O(n)\text{ model: }& \left\{V_{\langle r_0,1\rangle}\right\}_{r_0\in 2\mathbb{N}+1} 
 \cup \left\{ V^N_{(r,s)}\right\}_{\substack{s\in\frac12\mathbb{N}^* \\ r \in\frac{1}{s}\mathbb{Z}}} \ ,
 \label{opf}
\end{align}
i.e. diagonal degenerate fields and non-diagonal fields.

\begin{hyp}
The primary field $V^N_{(0, s)}$ with $s\in \frac12\mathbb{N}^*$ corresponds to a vertex with $\ell=2s$ lines.
\end{hyp}
Notice that we have a $\mathbb{Z}_\ell$ cyclic symmetry which permutes the lines around a vertex.
\begin{align}
 \begin{tikzpicture}[baseline=(current  bounding  box.center), scale = .4]
  \draw (0, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (0, 0) -- (100:2);
  \draw (0, 0) -- (60:2);
  \draw (0, 0) -- (20:2);
  \draw (0, 0) -- (-100:2);
  \node at (-.3, 2.6) {$1$};
  \node at (1.1, 2.3) {$2$};
  \node at (2.4, .7) {$3$};
  \node at (-.3,-2.6) {$\ell$};
  \draw[thick, dotted] ([shift = (10:1.4)]0, 0) arc (10:-90:1.4);
 \end{tikzpicture}
\end{align}
We know that a correlation function vanishes unless $\sum s_i\in\mathbb{Z}$. Let us focus on correlation functions of the type $\left<\prod_{i=1}^N V^N_{(0,s_i)}\right>$ with $\sum s_i\in \mathbb{N}$. This corresponds to inserting $N$ vertices with an even total number of lines $2\sum s_i\in 2\mathbb{N}$. The idea is that any line should be connected to another line from a different vertex. (Vertices are prevented from connecting to themselves by a condition of tracelessness of the corresponding tensor.) 

\begin{defn}
 A connectome is the specification of which lines connect with one another, such that no vertex is self-connected, and all lines can be drawn without crossing.
\end{defn}

A connectome is not completely specified by saying how many lines join any pair of vertices. The order of lines also matters. Here is an example of two inequivalent connectomes.
\begin{align}
 \begin{tikzpicture}[baseline=(current  bounding  box.center), scale = .4]
  \draw (0, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (0, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \node[below left] at (0, 0) {$1$};
  \node[above left] at (0, 3) {$2$};
  \node[above right] at (3, 3) {$3$};
  \node[below right] at (3, 0) {$4$};
 \end{tikzpicture}
 \qquad\qquad
 \begin{tikzpicture}[baseline=(current  bounding  box.center), scale = .4]
  \draw (0, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (0, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (0, 0) -- (0, 3) -- (3, 3) -- (3, 0) -- (0, 3) to [out = -60, in = 180] (3.2, -1) to [out = 0, in = -75] (3, 3) to [out = 160, in = 45] (-.5, 3.5) to [out = -135, in = 110] (0, 0);
  \node at (1.5, -2) {$[23, 1343, 1242, 23]$};
 \end{tikzpicture}
 \qquad\qquad
 \begin{tikzpicture}[baseline=(current  bounding  box.center), scale = .4]
  \draw (0, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (0, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (0, 0) -- (0, 3) -- (3, 3) -- (3, 0) -- (0, 3) to [out = -20, in = -160] (3, 3) to [out = 160, in = 45] (-.5, 3.5) to [out = -135, in = 110] (0, 0);
  \node at (1.5, -2) {$[23, 1334, 1422, 23]$};
 \end{tikzpicture}
\end{align}

\begin{hyp}
 There is a one-to-one correspondence between connectomes and linearly independent solutions of crossing symmetry.
\end{hyp}

\begin{defn}
 A connectome realization is a set of non-interesecting lines that realize a connectome.
\end{defn}


\begin{defn}
 A bipartition $X$ is a partition $\{1,2,\dots,N\}=X^1\sqcup X^2$ such that $\#X^i\geq 2$.
 Two bipartitions $X,Y$ are compatible if they are not identical, and $\exists i,j, X^i\cap Y^j=\emptyset$.
A channel is a set of $N-3$ compatible bipartitions. 
 \end{defn}
 
For example, in the case $N=4$, the $u$-channel is the bipartition $\{1,2,3,4\}=\{1,3\}\sqcup\{2,4\}$. 

\begin{defn}
 A channel realization of a given channel $\{X_1,X_2,\dots , X_{N-3}\}$ on the sphere with $N$ vertices $Z=\{z_1,z_2,\dots ,z_N\}$ is a set of $N-3$ non-intersecting closed contours $\{C_1,C_2,\dots , C_{N-3}\}$ such that $X_i$ describes how $C_i$ splits $Z$ into two subsets.  
\end{defn}
 
 For example, in the case $N=5$, let us draw a channel as a traditional tree diagram, and then one of its realizations:
 
 \begin{align}
  \begin{tikzpicture}[baseline=(current  bounding  box.center), scale = .4]
   \draw (-1, 2) node [left] {$2$} -- (0, 0) -- (4, 0) -- (5, 2) node [right] {$4$};
   \draw (2, 2) node [above] {$3$} -- (2, 0);
   \draw (-1, -2) node [left] {$1$} -- (0, 0);
   \draw (5, -2) node [right] {$5$}  -- (4, 0);
  \end{tikzpicture}
\hspace{4cm}
  \begin{tikzpicture}[baseline=(current  bounding  box.center), scale = .4]
  \draw (0, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (0, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (6, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (6, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \node[below left] at (0, 0) {$1$};
  \node[above left] at (0, 3) {$2$};
  \node[above] at (3, 3) {$3$};
  \node[above right] at (6, 3) {$4$};
  \node[below right] at (6, 0) {$5$};
  \draw[red] (0, 1.5) ellipse (2 and 3.5);
  \draw[red] (6, 1.5) ellipse (2 and 3.5);
 \end{tikzpicture}
 \end{align}

There are (infinitely) many topologically inequivalent realizations of a given channel. In the case of the $u$-channel, here are two examples:

\begin{align}
\begin{tikzpicture}[baseline=(current  bounding  box.center), scale = .4]
  \draw (0, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (0, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw[rotate = 45, red] (2.1, 0) ellipse (3 and 1); 
 \end{tikzpicture}
\hspace{4cm}
\begin{tikzpicture}[baseline=(current  bounding  box.center), scale = .4]
  \draw (0, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (0, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw[red] (0, -.5) to [out = 180, in = -135] (-1, 4) to [out = 45, in = 90] (3.5, 3) to [out = -90, in = -90] (2.5, 2.7) to [out = 90, in =45] (-.5, 3.5) to [out = -135, in = 180] (.3, .5) to [out =0, in =0] cycle;
 \end{tikzpicture}
\end{align}
 
This topological ambiguity in channel realizations will not affect our predicted spectrums, because of a corresponding topological ambiguity in connectome realizations.

\begin{hyp}
 The spectrum of a crossing-symmetric four-point function in a given channel can be inferred from the intersection of a channel realization with all possible connectome realizations. 
\end{hyp}

\begin{hyp}
 The index $s$ that appears in the spectrum is half the number of intersection points. If there are no intersection points, we obtain the degenerate sector. 
\end{hyp}

For example, let us plot a few realizations of the connectome $[2,1,4,3]$:
\begin{align}
\newcommand{\sconnr}{
 \draw (0, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 0) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (0, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw (3, 3) node[fill, circle, minimum size = 2mm, inner sep = 0]{};
  \draw[red] (0, 1.5) ellipse (1 and 2);}
 \begin{tikzpicture}[baseline=(current  bounding  box.center), scale = .4]
  \sconnr
  \draw (0, 0) -- (0, 3);
  \draw (3, 0) -- (3, 3);
 \end{tikzpicture}
 \hspace{2cm}
 \begin{tikzpicture}[baseline=(current  bounding  box.center), scale = .4]
  \sconnr
  \draw (0, 0) to [out = 45, in = -90] (2, 1.5) to [out = 90, in = -45] (0, 3);
  \draw (3, 0) -- (3, 3);
 \end{tikzpicture}
 \hspace{2cm}
 \begin{tikzpicture}[baseline=(current  bounding  box.center), scale = .4]
  \sconnr
  \draw (0, 0) to [out = -20, in = -90] (4, 0) to [out = 90, in = -30] (0, 3);
  \draw (3, 3) to [out = 160, in = 90] (-1, 3) to [out = -90, in = 150] (3, 0);
 \end{tikzpicture}
\end{align}
We interpret these realizations in terms of the presence of the degenerate sector, and sectors with $s=1,2$ respectively. Of course we can get $s\in \mathbb{N}^*$. Notice that two topologically equivalent realizations of the connectome do not necessarily correspond to the same states in the spectrum.


\subsection{To be done}

Understand the other index $r$ of representations. Is it related to the orientation of lines as they cross the channel realization?

Describe more general correlation functions, with not just fields of the type $V^N_{(0,s)}$.

Recover Hubert conjectures and more. Recover Rongvoram observations? 

Fusion rules and their associativity.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






\subsection{The index $r$ in terms of states on the cylinder and the torus partition function}
Relating to the first question in the above section. Instead of considering the index $r$ separately, let us consider the conformal spin $sr$ (considering here the non-diagonal case $j\neq 0$). Knowing $s$ we can then reverse-engineer what $r$ corresponds to in terms of diagrams. If we consider states on a cylinder, the conformal spin should indicate not how lines cross the ``seam'' but what \emph{would} be the effect (in terms of a phase) \emph{if} the cylinder were twisted before being glued into a torus. Going back to the lattice TL algebra this is determined by: 1) the number of through-lines $j$ and the boundary condition $e_\phi$, and 2) the lattice momentum $e$ which parametrizes for instance the difference between considering the state $\vcenter{\hbox{
	\begin{tikzpicture}
	\newcommand{\arcsize}{0.2}
 	\draw[thick, dotted] (-\arcsize -0.05,-\arcsize - 0.05) -- (-\arcsize-0.05,  0.05);
  	\draw[thick, dotted] (3*\arcsize + 0.05,-\arcsize - 0.05) -- (3*\arcsize+ 0.05,  0.05);
  	\draw[thick, dotted] (-\arcsize - 0.05,  0.05) -- (3*\arcsize+ 0.05,  0.05);
  	\draw[thick, dotted] (-\arcsize - 0.05, -\arcsize - 0.05) -- (3*\arcsize+ 0.05, -\arcsize - 0.05);
	%%%%
	\draw[thick] (0,0) arc (-180:0:\arcsize);
	\end{tikzpicture} }}
+
\vcenter{\hbox{
	\begin{tikzpicture}
	\newcommand{\arcsize}{0.2}
 	\draw[thick, dotted] (-\arcsize -0.05,-\arcsize - 0.05) -- (-\arcsize-0.05,  0.05);
  	\draw[thick, dotted] (3*\arcsize + 0.05,-\arcsize - 0.05) -- (3*\arcsize+ 0.05,  0.05);
  	\draw[thick, dotted] (-\arcsize - 0.05,  0.05) -- (3*\arcsize+ 0.05,  0.05);
  	\draw[thick, dotted] (-\arcsize - 0.05, -\arcsize - 0.05) -- (3*\arcsize+ 0.05, -\arcsize - 0.05);
	%%%%
	\draw[thick] (0,0) arc (0:-90:\arcsize);
	\draw[thick] (\arcsize*2,0) arc (-180:-90:\arcsize);
	\end{tikzpicture} }} 
$ (which is invariant under translation by one lattice site) and the state $\vcenter{\hbox{
	\begin{tikzpicture}
	\newcommand{\arcsize}{0.2}
 	\draw[thick, dotted] (-\arcsize -0.05,-\arcsize - 0.05) -- (-\arcsize-0.05,  0.05);
  	\draw[thick, dotted] (3*\arcsize + 0.05,-\arcsize - 0.05) -- (3*\arcsize+ 0.05,  0.05);
  	\draw[thick, dotted] (-\arcsize - 0.05,  0.05) -- (3*\arcsize+ 0.05,  0.05);
  	\draw[thick, dotted] (-\arcsize - 0.05, -\arcsize - 0.05) -- (3*\arcsize+ 0.05, -\arcsize - 0.05);
	%%%%
	\draw[thick] (0,0) arc (-180:0:\arcsize);
	\end{tikzpicture} }}
-
\vcenter{\hbox{
	\begin{tikzpicture}
	\newcommand{\arcsize}{0.2}
 	\draw[thick, dotted] (-\arcsize -0.05,-\arcsize - 0.05) -- (-\arcsize-0.05,  0.05);
  	\draw[thick, dotted] (3*\arcsize + 0.05,-\arcsize - 0.05) -- (3*\arcsize+ 0.05,  0.05);
  	\draw[thick, dotted] (-\arcsize - 0.05,  0.05) -- (3*\arcsize+ 0.05,  0.05);
  	\draw[thick, dotted] (-\arcsize - 0.05, -\arcsize - 0.05) -- (3*\arcsize+ 0.05, -\arcsize - 0.05);
	%%%%
	\draw[thick] (0,0) arc (0:-90:\arcsize);
	\draw[thick] (\arcsize*2,0) arc (-180:-90:\arcsize);
	\end{tikzpicture} }} 
$ (which acquires a sign).

We see that these are indeed the three ingredients that enter into $rs$. Since $j$ enters already into $s$, $e$ and $e_\phi$ must enter into $r$. Note that in the above reasoning we never make reference to whether a line crosses the ``seam'', only to what phase would be acquired if we were to twist the cylinder, with contributions from lattice momentum and from the through-lines changing their winding. This picture would make it seem impossible to judge what 4-point structure constants should vanish based directly on what the diagrams look like. 





%Appendixes
\renewcommand{\thesection}{A}

\appendix

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Bibliography

\bibliographystyle{morder6}

\bibliography{992}


\end{document}

